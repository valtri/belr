Name:           belr
Version:        4.4.34
Release:        1%{?dist}
Summary:        ABNF Parser

License:        GPLv3+
URL:            https://github.com/BelledonneCommunications/%{name}
Source0:        https://github.com/BelledonneCommunications/%{name}/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz
Patch0:         %{name}-pkgconfig.patch

BuildRequires:  bctoolbox-devel
BuildRequires:  cmake
BuildRequires:  gcc-c++

%description
Belr is Belledonne Communications' language recognition library, written in
C++11. It parses text inputs formatted according to a language defined by an
ABNF grammar, such as the protocols standardized at IETF.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        tests
Summary:        Tests for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    tests
The %{name}-tests package contains testsuite for Belr.


%prep
%autosetup -p1


%build
%cmake -DENABLE_STATIC=OFF
%cmake_build


%install
%cmake_install


%check
ln -sv $(pwd)/tester/res/ %{_vpath_builddir}/tester/
cd %{_vpath_builddir}/tester
./belr_tester


%{?ldconfig_scriptlets}


%files
%license LICENSE.txt
%doc CHANGELOG.md
%doc README.md
%{_bindir}/belr-compiler
%{_bindir}/belr-parse
%{_libdir}/libbelr.so.1

%files devel
%{_includedir}/%{name}/
%{_libdir}/libbelr.so
%{_libdir}/cmake/%{name}/
%{_libdir}/pkgconfig/%{name}.pc

%files tests
%{_bindir}/belr_tester
%{_datadir}/belr-tester/
%{_datadir}/%{name}/grammars/belr-grammar-example.blr
